app.controller("Admin", ["$scope", '$http' , function($scope, $http){

	// Leads variable points to admin's table view
	$scope.leads = [];

	// Function to retrieve all signed leads
	$scope.getLeads = function(){
		var params = {
			'action' : 'getLeads'
		}

		$http.post(EndPoint, params)
		.success(function(data){
			// console.log(data);
			$scope.leads = data;
		});
	}

}]);