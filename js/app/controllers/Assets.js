/*
	Assets controller needs to client side to register new leads in database
*/

app.controller("Assets", ["$scope", '$http', 'Validation' , function($scope, $http, Validation){
	$('#myModal').modal({ show: false});

	// Get countries function to retrieve countries for select element
	$scope.getCountries = function(){
		var params = {
			'action' : 'getCountries'
		}

		$http.post(EndPoint, params)
		.success(function(data){
			$scope.countries = data;
		});
	}

	// Inherit Validation class
	$scope.validate	 = Validation;	

	$scope.countries = [];
	$scope.fullname  = { text:'' , isvalid:false };
	$scope.phone	 = { text:'' , isvalid:false };
	$scope.email	 = { text:'' , isvalid:false };
	$scope.country	 = { text:'' , isvalid:false };
	
	// Register function sends request to server to save new Lead
	$scope.register = function(){
		var params = {
			'action' 	: 'Register',
			'fullname' 	: $scope.fullname.text,
			'phone' 	: $scope.phone.text,
			'email' 	: $scope.email.text,
			'countryID' : $scope.country.text
		}
// console.log(params);
		$http.post(EndPoint, params)
		.success(function(data){

			// If server response is 'ok' - All OK
			if(data.match('ok')){
				$("#myModalok").modal('show');
				window.location.reload();
			}
			else
				$("#myModalnull").modal('show');
			
		});
	}
	
	// Turn on Bootstrap tooltip feature
	$("[data-toggle=\"tooltip\"]").tooltip();

}]);
