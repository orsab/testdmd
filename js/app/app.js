/*
	Angular core named 'App' for validation, sending requests to server
*/

var app = angular.module("App", []);
var EndPoint = "/test/server/endpoint.php";	// API 'endpoint' url


// Validation - this is a static class to apperate 
// validations on a form before sending to server
app.factory('Validation', function(){
	
	var checkPhone = function(input){
		if(input.text == undefined || input.text == '')
			return;
		// console.log(input);

		var res = returnFunc(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/, input);

		return res ? 'has-success' : 'has-error';
	}

	var checkEmail = function(input){
		if(input.text == undefined || input.text == '')
			return;

		var res = returnFunc(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/, input);

		return res ? 'has-success' : 'has-error';
	}

	var checkCombo = function(input){
		if(input.text == undefined || input.text == '')
			return '';

		return 'has-success';
	}

	var checkEmpty = function(arr){
		var f = true;
		// console.log(arr);
		for(index in arr){
			var item = arr[index];
			if(item != undefined && item.text != undefined && item.text != '' && item.isvalid){
				f = false;
			}
			else{
				f = true;
				return f;
			}
		}

		return f;
	}

	function returnFunc(regex, input){
		var res = regex.test(input.text);
		input.isvalid = res;
		// console.log(input);

		return res;
	}

	return {
		'checkCombo' : checkCombo,
		'checkEmpty' : checkEmpty,
		'checkPhone' : checkPhone,
		'checkEmail' : checkEmail
	}
});