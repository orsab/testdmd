<?php 

/*
****	Basic protection against PostMan's
****	Cookies requires
*/
session_start();
$_SESSION['NLead'] = '';


 ?>

 <!DOCTYPE html>
 <html lang="en" ng-app="App">
 <head>
 	<meta charset="UTF-8">
 	<title>FTRADE | land</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
	<script src= "js/angular/angular.min.js"></script>
	<script src= "js/app/app.js"></script>
	<script src= "js/app/controllers/Assets.js"></script>
 </head>

 <!-- Area of Assets controller -->
 <body ng-controller="Assets">
 	<div>
		<header class="container" >
			<div class="col-xs-12">
				<div class="row">
					<div class="col-md-6 hidden-sm"></div>
					<div class="col-md-6 col-sm-12">
						<ul class="breadcrumb">
							<li><a href="#">LIVE CHAT!</a></li>
							<li><a href="#">WWW.FTRADE.COM</a></li>
							<li><a href="#">TOLL FREE +44 800 170 0599</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 hidden-sm hidden-md hidden-lg"></div>
					<div class="col-xs-4 col-md-4">
						<a href="#"><image src="img/logo.png" class="logo img-responsive"></image></a>
					</div>
					<div class="col-xs-4 hidden-sm hidden-md hidden-lg"></div>
				</div>
			</div>
		</header>

		<div class="col-xs-12 bg">
			<div class="col-xs-1"></div>
			<div class="col-xs-11 col-sm-6 toptext">
				<div class="col-xs-12">
					<span>Up to 81 Revenue in Less than 60 seconds!</span>
					<p>Trade Binary Options with FTrade and experience the best trading platform in the financial market. Trade dozens of currency pairs, commodities, indices and stocks, offering the highest payouts online!</p>
				</div>
				<img src="img/Rectangle_22.png" alt="">
				<div class="col-xs-12">
					<span>Make a quick and easy profit!</span>
					<p>60 Second Options are optimized for investors who want to take advantage of every market fluctuate. Your only goal is to predict the asset’s rate direction in the next minute. </p>
				</div>
			</div>
			<div class="col-xs-1"></div>
		</div>
		<div class="col-sm-12 hidden-xs img"></div>

		<!-- Input Fields Start -->
		<div class="col-xs-12 bottom-menu">
			<div class="row">
				<h4>FEW STEPS AWAY FROM SUCCESS</h4>
				<form novalidate role="form" class="form-group" name="form">
					<div class="col-sm-4 col-xs-8">
						<div class="topfield col-md-6">
							<div class=" has-feedback has-feedback-left" ng-class="form.fullname.$pristine ? '' : form.fullname.$dirty && form.fullname.$error.required ? 'has-error' : 'has-success' ">
								<input data-toggle="tooltip" data-placement="top" title="Required field" ng-change="fullname.text !='' ? fullname.isvalid=true : fullname.isvalid=false" name="fullname" ng-model="fullname.text" type="text" class="form-control" placeholder="Full Name" required />
								<i class="form-control-feedback glyphicon glyphicon-user"></i>
							</div>						
						</div>
						<div class="topfield col-md-6">
							<div class="has-feedback has-feedback-left" ng-class="validate.checkPhone(phone)" >
								<input data-toggle="tooltip" data-placement="top" title="Enter right format, like (123) 456-7890 123-456-7890 123.456.7890 1234567890" name="phone" ng-model="phone.text" type="text" class="form-control" placeholder="Phone" required/>
								<i class="form-control-feedback glyphicon glyphicon-earphone"></i>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-xs-8">
						<div class="topfield col-md-6">
						<div class=" has-feedback has-feedback-left"  ng-class="validate.checkEmail(email)">
							<input data-toggle="tooltip" data-placement="top" title="Enter email in right format" name="mail" ng-model="email.text" type="text" class="form-control" placeholder="Mail" required />
							<i class="form-control-feedback glyphicon glyphicon-envelope"></i>
						</div>
						</div>
						<div class="topfield col-md-6">
							<div class="has-feedback has-feedback-left" ng-class="validate.checkCombo(country)" ng-init="getCountries()">
								<select data-toggle="tooltip" data-placement="top" title="Choose your country" ng-model="country.text" ng-change="country.text !='' ? country.isvalid=true : country.isvalid=false" name="country" class="form-control">
									<option value="">Country</option>
									<option ng-repeat="country in countries" value="{{country.id}}">{{country.caption}}</option>
								</select>
								<i class="form-control-feedback glyphicon glyphicon-globe"></i>
							</div>
						</div>
					</div>
					<div class="col-xs-4 topfield">
						<button ng-click="register()" ng-disabled="validate.checkEmpty([phone,email,country,fullname]) || !conditions" class="btn btn-success submitbtn">JOIN FTRADE</button>
						<label data-toggle="tooltip" data-placement="top" title="Must accept our conditions"  class="checkbox-inline"><input ng-model="conditions" type="checkbox" >I accept the Terms and the Condition</label>
					</div>
				</form>
			</div>
		</div>
		<!-- Input Fields End -->
	</div>
	
	<!-- Content Block -->
	<content class="container content">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<div class="col-md-5 col-md-offset-1 col-xs-12">
					<h2>Trading Binary in 3 simple steps!</h2>
					<ol>
						<li>Trading Binary in 3 simple steps!</li>
						<li>Insert the investment amount</li>
						<li>Choose the asset’s direction and wait for expiry</li>
					</ol>
				</div>
				<div class="col-md-5 col-md-offset-1 col-xs-12">
					<img class="img-responsive" src="img/1-2-3.png" alt="">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 divider">
				<img width="100%" height="3px" src="img/Rectangle.png" alt="">
			</div>
		</div>

		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<div class="col-md-5 col-md-offset-1 col-xs-12">
					<img class="img-responsive" src="img/comp.png" alt="">
				</div>
				<div class="col-md-5 col-md-offset-1 col-xs-12">
					<p>FTrade enables its traders a beneficial financial development by providing the best Binary Options trading platform on the market. </p>
					<p class="lead"><b>Join now and relish the perfect trading environment!</b></p>
					<div class="col-sm-10 col-sm-offset-1">
						<button ng-click="register()" ng-disabled="validate.checkEmpty([phone,email,country,fullname]) || !conditions" class="btn btn-success submitbtn">JOIN FTRADE</button>
					</div>
					<div class="col-sm-10">
						<img class="img-responsive" src="img/money.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</content>
	<!-- End Content Block -->
	
	<!-- Footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 divider">
					<img width="100%" height="3px" src="img/Rectangle_1.png" alt="">
				</div>
			</div>
			<div class="row">

				<div class="col-xs-8">
					<ul class="breadcrumb">
						<li><a href="#">WWW.FTRADE.COM</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Disclaimer</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Terms & Conditions</a></li>
					</ul>
				</div>

			</div>
			<div class="divider black">
				<span>Risk Warning: Trading in Forex and Contracts for Difference (CFDs) is highly speculative and involves a significant risk of loss. The information contained in this publication is not intended as an offer or solicitation for the purchase or sale of any financial instrument. <a href="#">Read More</a> </span>
			</div>
		</div>
	</footer>
	<!-- End Footer -->

	<!-- Modals -->
	<div class="modal fade" id="myModalok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">FTRADE message</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="alert alert-success">
			  <strong>Success!</strong> Thank you for registration.
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="myModalnull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">FTRADE message</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="alert alert-danger">
			  <strong>Caution!</strong> Some of parameters are not valid.
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- End Modals -->

 </body>
 </html>