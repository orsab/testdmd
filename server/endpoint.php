<?php 
/*
	This is simple JSON API for Angular client
	All inputs and connections to database are protected by PDO and there 'prepare' function
*/

session_start();
if(!isset($_SESSION['NLead']))	// Basic Protection agains PostMan's
	die('Bad request');

require_once "functions.php";

// Take all data from raw post data
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

// Simple JSON response after JSON request
if(isset($request->action)){
	switch($request->action){
		case "getCountries":
			die(json_encode( getCountries() ));
		case "getLeads":
			die(json_encode( getLeads() ));
		case "Register":
			die(json_encode( Register($request->fullname, $request->phone, $request->email, $request->countryID) ));
	}
}

 ?>