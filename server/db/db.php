<?php
require_once "config.php";

class myPDO extends PDO{ 
	
	public function Insert($sql, $params = null){
		$stmt = parent::prepare($sql);
		// if(is_array($params))
			foreach($params as $field=>&$value){
				if(is_numeric($value)){
					$par = ":".$field;
					$stmt->bindParam($par, $value, PDO::PARAM_INT);
				}
				else{
					$par = ":".$field;
					$stmt->bindParam($par, $value, PDO::PARAM_STR);
				}
			}
		$stmt->execute();
		// var_dump($stmt->debugDumpParams());die;
		
		// return $stmt->fetchAll();
		return parent::lastInsertId();
	}
	
	public function Query($sql, $params = null){
		$stmt = parent::prepare($sql);
		// if(is_array($params))
		if($params != null){
			foreach($params as $field=>&$value){
				if(is_numeric($value)){
					$par = ":".$field;
					$stmt->bindParam($par, $value, PDO::PARAM_INT);
				}
				else{
					$par = ":".$field;
					$stmt->bindParam($par, $value, PDO::PARAM_STR);
				}
			}
		}
		$stmt->execute();
		// var_dump($stmt->debugDumpParams());die;
		
		return $stmt->fetchAll();
		// return parent::lastInsertId();
	}
}

class DB { 
    
    private static $objInstance; 
    
    /* 
     * Class Constructor - Create a new database connection if one doesn't exist 
     * Set to private so no-one can create a new instance via ' = new DB();' 
     */ 
    private function __construct() {} 
    
    /* 
     * Like the constructor, we make __clone private so nobody can clone the instance 
     */ 
    private function __clone() {}  
    
    /* 
     * Returns DB instance or create initial connection 
     * @param 
     * @return $objInstance; 
     */ 
    public static function getInstance() {
        if(!self::$objInstance){ 
            self::$objInstance = new myPDO(DB_DSN, DB_USER, DB_PASS,array(PDO::ATTR_TIMEOUT => "5",
																		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
																		PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8;SET time_zone='+03:00'")); 
            self::$objInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        }
        
        return self::$objInstance; 
    
    } # end method 
}

$dbcon = DB::getInstance();
?>