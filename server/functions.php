<?php 
/*
	Functions of API
*/


require_once "db/db.php";

// Get all leads for Admin Panel
function getLeads(){
	global $dbcon;

	$sql = "SELECT l.fullname, l.phone, l.email, l.createtime, c.caption as country
			FROM leads l
			INNER JOIN countries c
			ON c.id = l.countryID";
	$res = $dbcon->Query($sql);

	return $res;
}

// Get countries for select
function getCountries(){
	global $dbcon;

	$sql = "SELECT id, caption
			FROM countries";
	$res = $dbcon->Query($sql);

	return $res;
}

// Lead insertion function
function Register($fullname, $phone, $email, $countryID){
	global $dbcon;

	$sql = "INSERT INTO leads(fullname, phone, email, countryID)
			VALUES (:fullname, :phone, :email, :countryID)";

	$res;
	try{
		$res = $dbcon->Insert($sql, array(
			'fullname' 	=> $fullname,
			'phone' 	=> $phone,
			'email' 	=> $email,
			'countryID' => $countryID
			));
	}
	catch(PDOException $e){
		var_dump($e);
	}

	return $res > 0 ? 'ok' : 'null';
}


 ?>