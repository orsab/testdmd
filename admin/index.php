<?php 


session_start();
$_SESSION['NLead'] = '';



 ?>

 <!DOCTYPE html>
 <html lang="en" ng-app="App">
 <head>
 	<meta charset="UTF-8">
 	<title>Admin panel</title>
 	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome.min.css">

    <script src="../js/jquery/jquery.min.js"></script>
    <script src="../js/bootstrap/bootstrap.min.js"></script>
	<script src= "../js/angular/angular.min.js"></script>
    <link rel="stylesheet" href="../css/style.css">
	<script src= "../js/app/app.js"></script>
	<script src= "../js/app/controllers/Admin.js"></script>
 </head>
 <body ng-controller="Admin" ng-init="getLeads()">
 	
	<div class="container">
		<h2>New Leads</h2>
		<p>Test application</p>  
		
		<div class="col-xs-3">
			<input type="text" ng-model="search" class="form-control " placeholder="Type to search">          
		</div>

		<table class="table table-hover">
		    <thead>
		    	<th>Full name</th>
	    		<th>Phone</th>
	    		<th>Email</th>
	    		<th>Country</th>	
	    		<th>Created Time</th>	
			</thead>
			<tbody>
				<tr ng-repeat="lead in leads | filter : search">
					<td>{{lead.fullname}}</td>
					<td>{{lead.phone}}</td>
					<td>{{lead.email}}</td>
					<td>{{lead.country}}</td>
					<td>{{lead.createtime}}</td>
				</tr>
			</tbody>
		</table>
 </body>
 </html>